import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { MainProvider } from './providers/main';

ReactDOM.render(<MainProvider>
    <App />
</MainProvider>, document.getElementById('root'));

serviceWorker.unregister();
