import React, { useContext } from 'react'
import "./home.css"
import { Signin } from '../Signin'
import { MainContext } from '../../providers/main';

export const Home = () => {
    const context = useContext(MainContext);
    return (
        <div>
            <p className="lorem-blue">{context.websiteName}</p>
            <Signin></Signin>
        </div>
    )
}
