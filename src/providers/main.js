import React, { useState, createContext } from "react";

export const MainContext = createContext(null);

export const MainProvider = props => {
    const [websiteName, setWebsiteName] = useState("Express In Code")
    return (
        <MainContext.Provider
            value={{
                websiteName
            }}
        >
            {props.children}
        </MainContext.Provider>
    );
};
